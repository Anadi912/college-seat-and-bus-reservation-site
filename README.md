# COLLEGE SEAT AND BUS RESERVATION SITE
<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>CSS seat booking</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>

<meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  
      <link rel="stylesheet" href="/home/pc-59/Desktop/g.css">

  
</head>
<body>
<div class="plane">
  <div class="cockpit">
    <h1>Please select a seat</h1>
  </div>
  <div class="exit exit--front fuselage">
    
  </div>
  <ol class="cabin fuselage">
    <li class="row row--1">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="1" />
          <label for="1">1</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="2" />
          <label for="2">2</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="3" />
          <label for="3">3</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<li class="seat">
          <input type="checkbox" id="4" />
          <label for="4">4</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="5" />
          <label for="5">5</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="6" />
          <label for="6">6</label>
        </li>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<li class="seat">
          <input type="checkbox" id="7" />
          <label for="7">7</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="8" />
          <label for="8">8</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="9" />
          <label for="9">9</label>
        </li>
      </ol>
    </li>
    <li class="row row--2">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="10" />
          <label for="10">10</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="11" />
          <label for=11">11</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="12" />
          <label for="12">12</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<li class="seat">
          <input type="checkbox" id="13" />
          <label for="13">13</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="14" />
          <label for="14">14</label>
        </li>
         <li class="seat">
          <input type="checkbox" id="15" />
          <label for="15">15</label>
        </li
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <li class="seat">
          <input type="checkbox" id="10" />
          <label for="10">10</label>
        </li
        <li class="seat">
        <input type="checkbox" id="16" />
          <label for="16">16</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="17" />
          <label for="17">17</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="18" />
          <label for="18">18</label>
        </li>
      </ol>
    </li>
    <li class="row row--3">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="11" />
          <label for="11">11</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="12" />
          <label for="12">12</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="13" />
          <label for="13">13</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <li class="seat">
          <input type="checkbox" id="14" />
          <label for="14">14</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="15" />
          <label for="15">15</label>
        </li>
      </ol>
    </li>
    <li class="row row--4">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="16" />
          <label for="16">16</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="17" />
          <label for="17">17</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="18" />
          <label for="18">18</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        <li class="seat">
          <input type="checkbox" id="19" />
          <label for="19">19</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="20" />
          <label for="20">20</label>
        </li>
      </ol>
    </li>
    <li class="row row--5">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="21" />
          <label for="21">21</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="22" />
          <label for="22">22</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="23" />
          <label for="23">23</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        <li class="seat">
          <input type="checkbox" id="24" />
          <label for="24">24</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="25" />
          <label for="25">25</label>
        </li>
      </ol>
    </li>
    <li class="row row--6">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="26" />
          <label for="26">26</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="27" />
          <label for="27">27</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="28" />
          <label for="28">28</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        <li class="seat">
          <input type="checkbox" id="29" />
          <label for="29">29</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="30" />
          <label for="30">30</label>
        </li>
      </ol>
    </li>
    <li class="row row--7">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="31" />
          <label for="31">31</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="32" />
          <label for="32">32</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="33" />
          <label for="33">33</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        <li class="seat">
          <input type="checkbox" id="34" />
          <label for="34">34</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="35" />
          <label for="35">35</label>
        </li>
      </ol>
    </li>
    <li class="row row--8">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="36" />
          <label for="36">36</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="37" />
          <label for="37">37</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="38" />
          <label for="38">38</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        <li class="seat">
          <input type="checkbox" id="39" />
          <label for="39">39</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="40" />
          <label for="40">40</label>
        </li>
      </ol>
    </li>
    <li class="row row--9">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="41" />
          <label for="41">41</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="42" />
          <label for="42">42</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="43" />
          <label for="43">43</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        <li class="seat">
          <input type="checkbox" id="44" />
          <label for="44">44</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="45" />
          <label for="45">45</label>
        </li>
      </ol>
    </li>
    <li class="row row--10">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="46" />
          <label for="46">46</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="47" />
          <label for="47">47</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="48" />
          <label for="48">48</label>
        </li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        <li class="seat">
          <input type="checkbox" id="49" />
          <label for="49">49</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="50" />
          <label for="50">50</label>
        </li>
      </ol>
    </li>
  </ol>
  <div class="exit exit--back fuselage">
    
  </div>
</div>
</body>
</html>

